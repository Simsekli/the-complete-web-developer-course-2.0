var start = new Date().getTime();

        function makeShapeAppear() {
            var shapeDiv = document.getElementById("shape");
            shapeDiv.style.display = 'block';

            var top = Math.floor(50 + Math.random() * 400);
            var left = Math.floor(50 + Math.random() * 400);
            var width = Math.floor(50 + Math.random() * 300);

            shapeDiv.style.top = top + "px";
            shapeDiv.style.left = left + "px";
            shapeDiv.style.height = width + "px";
            shapeDiv.style.width = width + "px";
            shapeDiv.style.backgroundColor = getRandomColor();

            if(Math.random()<0.5){
                shapeDiv.style.borderRadius = "50%";
            }
            else{
                shapeDiv.style.borderRadius = "0%";
            }

            start = new Date().getTime();

        }

        function appearAfterDelay() {
            setTimeout(makeShapeAppear, Math.random() * 2000);
        }

        appearAfterDelay();


        var shape = document.getElementById("shape");
        shape.onclick = function () {
            shape.style.display = 'none';
            var end = new Date().getTime();

            var par = document.getElementById("elapsedTime");
            par.innerHTML = (end - start) + " millis";

            appearAfterDelay();
        }

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }